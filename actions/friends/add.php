<?php
/**
 * Elgg add friend action
 *
 * @package Elgg.Core
 * @subpackage Friends.Management
 * @override actions/friends/add.php
 */


// Get the GUID of the user to friend
$friend_guid = get_input('friend');
$friend = get_entity($friend_guid);
if (!$friend) {
	register_error(elgg_echo('error:missing_data'));
	forward(REFERER);
}
$user = elgg_get_logged_in_user_entity();

$errors = false;

if(check_entity_relationship($friend->guid, "friendrequest", $user->guid)
	|| check_entity_relationship($friend->guid, "friend", $user->guid)) {

	try {
		if (!$user->addFriend($friend_guid)) {
			$errors = true;
		}
    
        $ia = elgg_set_ignore_access(true);
        $friend->addFriend($user->guid);
        elgg_set_ignore_access($ia);
	
        remove_entity_relationship($friend->guid, "friendrequest", $user->guid);
    
	} catch (Exception $e) {
		register_error(elgg_echo("friends:add:failure", array($friend->name)));
		$errors = true;
	}
	if (!$errors) {
		// add to river
		add_to_river('river/relationship/friend/create', 'friend', $user->guid, $friend->guid);
		add_to_river('river/relationship/friend/create', 'friend', $friend->guid, $user->guid);
		system_message(elgg_echo("friends:add:successful", array($friend->name)));
	}
} elseif(add_entity_relationship($user->guid, "friendrequest", $friend->guid)) {
	system_message(elgg_echo("friendrequest:add:successful", array($friend->name)));
} else {
	register_error(elgg_echo("friendrequest:add:exists", array($friend->name)));
}

// Forward back to the page you friended the user on
forward(REFERER);
