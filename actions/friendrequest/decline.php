<?php

$user = elgg_get_logged_in_user_entity();
$friend = get_entity(sanitize_int(get_input("guid")));

if(remove_entity_relationship($friend->guid, 'friendrequest', $user->guid)) {
	system_message(elgg_echo("friendrequest:decline:success", array($friend->name)));
} else {
	system_message(elgg_echo("friendrequest:decline:fail"));
}

forward(REFERER);
