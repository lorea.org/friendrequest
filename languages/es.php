<?php
$es = array (
  'friendrequest' => 'Peticiones de amistad',
  'friendrequest:add:successful' => 'Has enviado una petición de amistad a %s. Tiene que  aprobarla antes de salir en tu lista de amigos. Dale tiempo :)',
  'friendrequest:add:failure' => 'Ups!, no se sabe muy bien por qué, pero el sistema no puede completar tu petición. Inténtalo de nuevo o ponte en contacto con lxs administradorxs.',
  'friendrequest:add:exists' => 'Ya has enviado antes una petición de amistad a %s. Ya lo leerá, ¡ten paciencia!',
  'friendrequest:decline:success' => 'Has rechazado la petición de amistad de %s',
  'friendrequest:decline:fail' => 'No ha sido posible borrar la petición de amistad. Inténtalo de nuevo o ponte en contacto con lxs administradorxs.',
  'friendrequest:remove:check' => 'Estás seguro de rechazar esta petición de amistad?',
  'friendrequest:none' => 'No hay peticiones de amistad pendientes.',
  'friendrequest:new:subject' => '%s quiere ser tu amigx',
  'friendrequest:new:body' => '%s quiere ser tu amigx. Está esperando a que apruebes su petición, no hagas que se impaciente ;)

Puedes ver las peticiones que tienes pendientes a través del siguiente link (recuerda que tienes estar logeadx en la red para poder hacerlo):

	%s

No puedes responder a este email porque soy una máquina y no te leeré, no lo intentes ;)',
);

add_translation("es", $es);

