<?php
/**
 * ElggFriendRequest galician language file.
 *
 */

$galician = array(
	'friendrequest' => 'Peticións de amizade',
	
	'friendrequest:add:successful' => 'Enviaches unha petición de amizade a %s. Ten que aprobala se quere, dálle tempo :)',
	'friendrequest:add:failure' => 'Mimá!, non se sabe moi ben por qué, pero o sistema non pode completa-la túa petición. Téntao de novo ou ponte en contacto cxs administran a rede.',
	'friendrequest:add:exists' => 'Xa lle tes enviado unha petición de amizade s %s. Xa o lerá, ten paciencia meu!',
	
	'friendrequest:decline:success' => 'Borrache-la petición de amizade',
	'friendrequest:decline:fail' => 'Non foi posible borra-la petición de amizade. Téntao de novo ou ponte en contacto coxs que administran a rede.',
	
//	'friendrequest:remove:check' => 'Are you sure you want to decline the friend request?',
//	'friendrequest:none' => 'No pending friend requests.',
	
	'friendrequest:new:subject' => '%s quere ter amizade contigo',
	'friendrequest:new:body' => '%s quere ter amizade contigo. Está a agardar a que aprobe-la súa petición, non fagas que se impaciente ;)

Si te logueaches na rede, podes ve-las peticións que tes pendentes a través da seguinte ligazón:

%s

Non podes responder a este email porque son unha máquina e non te vou ler, non o tentes ;)',
	
);

add_translation('gl', $galician);
