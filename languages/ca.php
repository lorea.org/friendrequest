<?php
$language = array (
  'friendrequest' => 'Sol·licituds d\'Amistat',
  'friendrequest:add:successful' => 'Has enviat una sol·licitud d\'amistat a %s. La ha d\'acceptar. Dóna-li temps.',
  'friendrequest:add:failure' => 'Oops! Hi ha hagut algun problema mentre s\'intentava enviar la sol·licitud. Torna-ho a provar.',
  'friendrequest:add:exists' => 'Ja has enviat la sol·licitud d\'amistat a %s, no siguis tan insistent.',
  'friendrequest:decline:success' => 'Has denegat la sol·licitud d\'amistat de %s',
  'friendrequest:decline:fail' => 'Per algun motiu aquesta petició d\'amistat no pot ésser eliminada',
  'friendrequest:remove:check' => 'Estas segur de que vols denegar la sol·licitud d\'amistat?',
  'friendrequest:none' => 'No hi ha sol·licitud d\'amistat pendents.',
  'friendrequest:new:subject' => '%s vol ser la teva amiga',
  'friendrequest:new:body' => '% vol ser la teva amiga. Està esperant que aprovis la seva sol·licitud. No la facis esperar!

Pots veure les teves sol·licituds d\'amistat pendents clicant a aquest link (recorda que has d\'estar loginat per veure-la):

%s

(No responguis aquest mail, perquè és una màquina qui l\'envia)',
);
add_translation("ca", $language);