<?php
/**
 * ElggFriendRequest English language file.
 *
 */

$english = array(
	'friendrequest' => 'Friend Requests',
	
	'friendrequest:add:successful' => 'You sent a friend request to %s. She has to accept it. Give she time.',
	'friendrequest:add:failure' => 'Oops! There was a problem while trying to send the request. Try it again.',
	'friendrequest:add:exists' => 'You already sent a friend request to %s, don\'t be so intense.',
	
	'friendrequest:decline:success' => 'You just declined %s friend request',
	'friendrequest:decline:fail' => 'For any reason this friend request cannot be deleted',
	
	'friendrequest:remove:check' => 'Are you sure you want to decline the friend request?',
	'friendrequest:none' => 'No pending friend requests.',
	
	'friendrequest:new:subject' => '%s want to be your friend',
	'friendrequest:new:body' => '%s want to be your friend. She is waiting that you aprovate her petition. Do not her wait!

You can see your pending friend requests following this link (remember you have to be logged in to see it):

	%s
	
(Do not try to reply this mail, because is a machine who is sending it)',
	
);

add_translation('en', $english);
