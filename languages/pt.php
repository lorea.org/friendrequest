<?php
/**
 * ElggFriendRequest Portuguese language file.
 *
 */

$portuguese = array(
	'friendrequest' => 'Pedidos de amizade',
	
	'friendrequest:add:successful' => 'Você enviou um pedido de amizade a &amp;s. Elx tem que aprova-la antes de aparecer na tua lista de amigxs. Espere um pouco :)',
	'friendrequest:add:failure' => 'Erro. Não foi possível completar seu pedido. Tente novamente ou entre em contato com xs administradorxs.',
	'friendrequest:add:exists' => 'Você já enviou um pedido de amizade a &amp;s. Elx Leerá em breve, seja paciente!',
	
	'friendrequest:decline:success' => 'O pedido de amizade foi apagado corretamente',
	'friendrequest:decline:fail' => 'Não foi possível apagar o pedido de amizade. Tente novamente ou entre em contato com xs administradorxs.',
	
//	'friendrequest:remove:check' => 'Are you sure you want to decline the friend request?',
//	'friendrequest:none' => 'No pending friend requests.',
	
	'friendrequest:new:subject' => '%s quer ser teu/tua amigx!',
	'friendrequest:new:body' => '%s quer ser teu/tua amigx. Elx está esperando que você aceite o pedido.

Você pode ver os teus pedidos pendentes clicando em (lembre-se que você tem que estar loga)):

%s

Não responda a esta mensagem porque eu sou uma máquina e não vou te ler ;)',
	
);

add_translation('pt', $portuguese);
