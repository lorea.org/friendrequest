<?php
/**
 * Elgg friend request plugin.
 *
 * @package ElggFriendRequest
 */

elgg_register_event_handler('init', 'system', 'friendrequest_init');

function friendrequest_init() {
	
	//This will let users view their friend requests
	elgg_register_page_handler('friendrequests', 'friendrequest_page_handler');
	
	elgg_register_plugin_hook_handler('register', 'menu:topbar', 'friendrequest_topbar_menu');
	elgg_register_plugin_hook_handler('register', 'menu:page', 'friendrequest_page_menu');
	
	//We need to override the friend remove action to remove the relationship we created
	$actions_dir = elgg_get_plugins_path().'friendrequest/actions/friends';
	elgg_register_action('friends/add', "$actions_dir/add.php");
	elgg_register_action('friends/remove', "$actions_dir/remove.php");
   	
   	$actions_dir = elgg_get_plugins_path().'friendrequest/actions/friendrequest';
	elgg_register_action('friendrequest/decline', "$actions_dir/decline.php");
   	
   	//Regular Elgg engine sends out an email via an event. The 400 priority will let us run first.
	//Then we return false to stop the event chain. The normal event handler will never get to run.
	//elgg_register_event_handler('create', 'friend', 'friendrequest_event_create_friend', 400);
	
	//Handle our add action event:
	elgg_register_event_handler('create', 'friendrequest', 'friendrequest_event_create_friendrequest');
	
}

function friendrequest_page_handler($page){
	
	//Keep the URLs uniform:
	if (isset($page[0])) {
		forward("friendrequests");
	}
	
	gatekeeper();
	
	elgg_push_context('friends');

	$title = elgg_echo('friendrequest');
	
	$page_owner = elgg_get_logged_in_user_entity();
	elgg_set_page_owner_guid($page_owner->guid);
		
	$requests = elgg_get_entities_from_relationship(array(
		'type' => 'user',
		'relationship' => 'friendrequest',
		'relationship_guid' => $page_owner->guid,
		'inverse_relationship' => true,
		'limit' => 0,
	));
	$content = elgg_view('friendrequest/requests', array(
		'requests' => $requests,
	));

	$params = array(
		'content' => $content,
		'title' => $title,
		'filter' => '',
	);
	$body = elgg_view_layout('content', $params);

	echo elgg_view_page($title, $body);
	
	return true;
}

function friendrequest_count(){
	return elgg_get_entities_from_relationship(array(
		'type' => 'user',
		'relationship' => 'friendrequest',
		'relationship_guid' => elgg_get_logged_in_user_guid(),
		'inverse_relationship' => true,
		'limit' => 0,
		'count' => true,
	));
}

function friendrequest_topbar_menu($hook, $entity_type, $returnvalue, $params) {
	$count = friendrequest_count();
	if($count) {
		foreach($returnvalue as $item) {
			if($item->getName() == 'friends') {
				$item->setText(
					$item->getText() . elgg_view('output/url', array(
						'href' => 'friendrequests',
						'text' => "<span class=\"messages-new\">$count</span>",
						'title' => elgg_echo('friendrequest'),
					))
				);
				return $returnvalue;
			}
		}
	}
	return $returnvalue;
}

function friendrequest_page_menu($hook, $entity_type, $returnvalue, $params) {
	if(elgg_in_context('friends')){
		foreach($returnvalue as $i => $item) {
			if($item->getName() == 'friends:of') {
				unset($returnvalue[$i]);
			}
		}
		if (elgg_is_logged_in()) {
			$count = friendrequest_count();		
			$item = new ElggMenuItem('friendrequests', elgg_echo('friendrequest').($count? " ($count)": ""), "friendrequests");
			$returnvalue[] = $item;
		}
	}
	return $returnvalue;
}

function friendrequest_event_create_friend($event, $object_type, $object){var_dump($object);elgg_echo('hola');
	if (($object instanceof ElggRelationship) && ($event == 'create') && ($object_type == 'friend')) {
		//We don't want anything happening here... (no email/etc)
		
		//Returning false will interrupt the rest of the chain.
		//The normal handler for the create friend event has a priority of 500 so it will never be called.	
		return false;
	}
	return true; //Shouldn't get here...
}

function friendrequest_event_create_friendrequest($event, $object_type, $object){
	if (($object instanceof ElggRelationship) && ($event == 'create') && ($object_type == 'friendrequest')) {
		$requester = get_entity($object->guid_one);
		$requested = get_entity($object->guid_two);

		$friendrequests_url = elgg_get_site_url() . "friendrequests/";

		// Notify target user
		return notify_user(
			$requested,
			$requester,
			elgg_echo('friendrequest:newfriend:subject', array(
				$requester->name
			), $requested->language), 
			elgg_echo('friendrequest:newfriend:body', array(
				$requester->name,
				$friendrequests_url,
			), $requested->language)
		);
	}
}
